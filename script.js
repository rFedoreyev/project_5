function click1() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    let res = f1[0].value * f2[0].value;
    r.innerHTML = res.toFixed(2);
    if (Number.isNaN(res)) {
        r.innerHTML = "Error";
    }
    return false;
}